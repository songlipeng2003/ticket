//
//  FJMapButton.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theatre.h"

@interface FJMapButton : UIButton{
    Theatre *theatre;
}

@property (nonatomic,strong) Theatre *theatre;

@end
