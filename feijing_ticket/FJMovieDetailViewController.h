//
//  FJMovieDetailViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import "Theatre.h"

@interface FJMovieDetailViewController : UITableViewController {
    Movie *movie;
    NSArray *theatres;
    BOOL byDistance;
    Theatre *selectedTheatre;
    
    UIView *movieInfoView;
    UIImageView *imageView;
    UILabel *titleLabel;
    UILabel *summaryLabel;
    UIButton *detailButton;
    UISegmentedControl *theatreListTypeControl;
}

@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) NSArray *theatres;
@property (nonatomic) BOOL byDistance;
@property (nonatomic, strong) Theatre *selectedTheatre;

@property (nonatomic, strong) IBOutlet UIView *movieInfoView;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *summaryLabel;
@property (nonatomic, strong) IBOutlet UIButton *detailButton;
@property (nonatomic, strong) IBOutlet UISegmentedControl *theatreListTypeControl;

- (void)configureView;
-(IBAction) changeTheatreListType: (UISegmentedControl *) sender;

@end
