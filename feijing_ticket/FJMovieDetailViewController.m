//
//  FJMovieDetailViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJMovieDetailViewController.h"
#import "UIExpandableTableView.h"
#import "GHCollapsingAndSpinningTableViewCell.h"
#import "FJTheatreMovieDetailViewController.h"

@implementation FJMovieDetailViewController

@synthesize movie;
@synthesize theatres;
@synthesize byDistance;
@synthesize selectedTheatre;

@synthesize movieInfoView;
@synthesize imageView;
@synthesize titleLabel;
@synthesize summaryLabel;
@synthesize detailButton;
@synthesize theatreListTypeControl;

- (void)setMovie:(Movie *)newMovie
{
    if (movie != newMovie) {
        movie = newMovie;
        
        // Update the view.
        [self configureView];
    };
}

- (void)configureView{
    // Update the user interface for the detail item.
    
    if (self.movie) {
        self.navigationItem.title = self.movie.title;
        //NSURL *url = [NSURL URLWithString:movie.image];
        //NSData *data = [NSData dataWithContentsOfURL:url];
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"movie" ofType:@"jpg"]];
        UIImage *image = [[UIImage alloc] initWithData:data];
        self.imageView.image = image;
        self.summaryLabel.text = movie.summary;
        self.titleLabel.text = movie.title;
    }
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView = [[UIExpandableTableView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 367.0f) style:UITableViewStylePlain];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

# pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section==0){
        return movieInfoView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0){
        return 260.0f;
    }
    return 0.0f;
}

# pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(byDistance){
        return 6;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"theatreInMovieCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(byDistance){
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        
        cell.textLabel.text = @"Theatre";
        cell.detailTextLabel.text = @"北京市中关村大街50号";
    }else{
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
        
        cell.textLabel.text = @"Theatre";
        cell.detailTextLabel.text = @"北京市中关村大街50号";
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(byDistance){
        return 3;
    }else{
        return 18;
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger index = [indexPath row];
    Theatre *theatre = [[Theatre alloc] init];
    
    NSString *number = [NSString stringWithFormat:@"%d", 0];
    
    theatre.name = [number stringByAppendingString:@" Theatre"];
    theatre.address = @"北京市中关村大街50号";
    theatre.phone = @"15010091144";
    theatre.longitude = [[NSNumber alloc] initWithFloat:(39.510518 + (float)(arc4random()%1000)/10000)];
    theatre.latitude = [[NSNumber alloc] initWithFloat:(116.09825 + (float)(arc4random()%1000)/10000)];
    
    selectedTheatre = [theatres objectAtIndex:index];
    selectedTheatre = theatre;
    
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"theatreToMovieTheatre" sender:self];
}

#pragma mark - UIExpandableTableViewDatasource

- (BOOL)tableView:(UIExpandableTableView *)tableView canExpandSection:(NSInteger)section {
    if(byDistance){
        return YES;
    }else {
        return NO;
    }
}

- (BOOL)tableView:(UIExpandableTableView *)tableView needsToDownloadDataForExpandableSection:(NSInteger)section {
    return NO;
}

- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(UIExpandableTableView *)tableView expandingCellForSection:(NSInteger)section {
    NSString *CellIdientifier = @"GHCollapsingAndSpinningTableViewCell";
    
    GHCollapsingAndSpinningTableViewCell *cell = (GHCollapsingAndSpinningTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdientifier];
    
    if (cell == nil) {
        cell = [[GHCollapsingAndSpinningTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdientifier];
    }
    
    switch (section) {
        case 0:
            cell.textLabel.text = @"海淀区";
            break;
        case 1:
            cell.textLabel.text = @"朝阳区";
            break;
        case 2:
            cell.textLabel.text = @"西城区";
            break;
        case 3:
            cell.textLabel.text = @"东城区";
            break;
        case 4:
            cell.textLabel.text = @"石景山区";
            break;
        case 5:
            cell.textLabel.text = @"昌平区";
            break;
        default:
            cell.textLabel.text = @"通州区";
            break;
    }
    
    return cell;
}

# pragma mark - 
-(IBAction) changeTheatreListType: (UISegmentedControl *) sender{
    //得到按钮点击索引   
    NSInteger index = sender.selectedSegmentIndex;
    
    if(index==1){
        byDistance = true;
    }else{
        byDistance = false;
    }
    
    [self.tableView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"theatreToMovieTheatre"]){
        FJTheatreMovieDetailViewController *theatreMovieDetailViewController = (FJTheatreMovieDetailViewController *)[segue destinationViewController];
        theatreMovieDetailViewController.movie = movie;
        theatreMovieDetailViewController.theatre = selectedTheatre;
    }
}

@end
