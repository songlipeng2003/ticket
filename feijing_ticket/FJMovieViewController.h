//
//  FJFirstViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FJMovieDetailViewController.h"
#import "FlowCoverView.h"

@interface FJMovieViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, FlowCoverViewDelegate>{
    NSMutableArray *movies;
    UIButton *cityButton;
    FlowCoverView *movieOpenFlowView;
    UITableView *moviewTableView;
    Movie *currentMovie;
}

@property (nonatomic, strong) NSMutableArray *movies;
@property (nonatomic, strong) Movie *currentMovie;

@property (nonatomic, strong) IBOutlet FlowCoverView *movieOpenFlowView;
@property (nonatomic, strong) IBOutlet UITableView *moviewTableView;
@property (nonatomic, strong) IBOutlet UIButton *cityButton;

-(IBAction) buttonAction: (UISegmentedControl *) sender;

@end
