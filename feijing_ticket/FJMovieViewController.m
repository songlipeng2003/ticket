//
//  FJFirstViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJMovieViewController.h"
#import "MovieParser.h"

@implementation FJMovieViewController

@synthesize movies;
@synthesize currentMovie;

@synthesize cityButton;
@synthesize movieOpenFlowView;
@synthesize moviewTableView;

- (void)didReceiveMemoryWarning 
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    //self.navigationController.delegate = self;
    
    // 获取电影票
//    NSURL *url = [NSURL URLWithString:@"http://p5.groupon.cn/xml/city/cityproduct/?cityid=7"];
//    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithContentsOfURL:url];
//    MovieParser *movieParser = [[MovieParser alloc] init];
//    [xmlParser setDelegate:movieParser];
//    [xmlParser parse];
//    
//    self.movies = [movieParser.movies copy];
    
    self.movies = [[NSMutableArray alloc] init];
    
    for(int i=0;i<10;i++){
        NSString *number = [NSString stringWithFormat:@"%d", i];
        Movie *movie = [[Movie alloc] init];
        
        movie.title = [number stringByAppendingString:@" Movie"];
        movie.summary = @"曾荣获奥斯卡金像奖的著名导演安德鲁·斯坦顿带来全新力作《异星战场》，一部以神秘壮丽的火星为背景的史诗动作冒险巨制。";
        movie.price = [[NSNumber alloc] initWithFloat:50];
        movie.image = @"http://img3.douban.com/view/photo/thumb/public/p1409731036.jpg";
        
        [self.movies addObject:movie];
    }
    NSInteger count =  [self.movies count];
    
    NSLog(@"cout of movies: %d", count);
    
    movieOpenFlowView.delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *currentCity = [[NSUserDefaults standardUserDefaults] stringForKey:@"currentCity"];
    currentCity = currentCity==nil?@"北京":currentCity;
    [cityButton setTitle:currentCity forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100.0f;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"movieTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Movie *movie = [movies objectAtIndex:[indexPath row]];
    
    cell.textLabel.text = movie.title;
    cell.detailTextLabel.text = movie.summary;
    [cell.detailTextLabel setLineBreakMode:UILineBreakModeWordWrap];
    [cell.detailTextLabel setNumberOfLines:0];
    //NSURL *url = [NSURL URLWithString:movie.image];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"movie" ofType:@"jpg"]];
    UIImage *image = [[UIImage alloc] initWithData:data];
    cell.imageView.image = image;
    //cell.imageView.frame = CGRectMake(0.0f, 0.0f, 40.0f, 80.0f);
    
    return cell;
}
                                                                               
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count =  [self.movies count];
    return count;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger index = [indexPath row];
    Movie *selectedMovie = [movies objectAtIndex:index];
    currentMovie = selectedMovie;
    
    return indexPath;
}

#pragma mark - FlowCoverViewDelegate
- (int)flowCoverNumberImages:(FlowCoverView *)view
{
	return movies.count;
}

- (UIImage *)flowCover:(FlowCoverView *)view cover:(int)image
{
    Movie *movie = [movies objectAtIndex:image];
	//NSURL *url = [NSURL URLWithString:movie.image];
    //NSData *data = [NSData dataWithContentsOfURL:url];
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"movie" ofType:@"jpg"]];
    UIImage *ui_image = [[UIImage alloc] initWithData:data];
    
    return ui_image;
}

- (void)flowCover:(FlowCoverView *)view didSelect:(int)image
{
	NSLog(@"Selected Index %d",image);
    
    Movie *selectedMovie = [movies objectAtIndex:image];
    currentMovie = selectedMovie;
    [self performSegueWithIdentifier:@"tableToMovieDetail" sender:self];
}

#pragma mark
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"tableToMovieDetail"]){
        id controller = segue.destinationViewController;
        
        if([controller isKindOfClass:[FJMovieDetailViewController class]]){
            FJMovieDetailViewController *movieDetailViewController = segue.destinationViewController;
            
            movieDetailViewController.movie = currentMovie;
        }
    }
}

-(IBAction) buttonAction: (UISegmentedControl *) sender  
{  
    //得到按钮点击索引   
    NSInteger index = sender.selectedSegmentIndex;
    
    if(index==1){
        self.view = movieOpenFlowView;
    }else{
        self.view = moviewTableView;
    }
}
                                                                               
@end
