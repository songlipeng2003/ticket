//
//  FJSelectCityViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FJSelectCityViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>{
    NSMutableArray *cities;
    NSString *currentCity;
}

@property (nonatomic, strong) NSMutableArray *cities;
@property (nonatomic, strong) NSString *currentCity;

@end
