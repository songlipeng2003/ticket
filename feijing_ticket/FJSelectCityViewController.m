//
//  FJSelectCityViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJSelectCityViewController.h"

@implementation FJSelectCityViewController

@synthesize cities;
@synthesize currentCity;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    cities = [[NSMutableArray alloc] initWithObjects:@"北京", @"上海", @"广州", @"深圳", @"西安", @"重庆", @"成都", nil];
    
    currentCity = [[NSUserDefaults standardUserDefaults] stringForKey:@"currentCity"];
    currentCity = currentCity==nil?@"北京":currentCity;
}

# pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier;
    
    if(indexPath.section==0){
        cellIdentifier = @"currentCity";
    }else{
        cellIdentifier = @"city";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(indexPath.section==0){
        cell.textLabel.text = currentCity;
    }else{
        NSString *city = [cities objectAtIndex:indexPath.row];
        cell.textLabel.text = city;
        
        if([currentCity isEqualToString:city]){
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }else{
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return 1;
    }
    
    return cities.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section==0){
        return @"当前城市";
    }else{
        return @"可选城市";
    }
}

#pragma - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section!=0){
        UITableViewCell *lastSelectCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[cities indexOfObject:currentCity] inSection:1]];
        lastSelectCell.accessoryType = UITableViewCellAccessoryNone;
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        currentCity = cell.textLabel.text;
        
        UITableViewCell *currencyCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        currencyCell.textLabel.text = currentCity;
        
        [[NSUserDefaults standardUserDefaults] setValue:currentCity forKey:@"currentCity"];
        
        [self.navigationController popViewControllerAnimated:TRUE];
    }
}

@end
