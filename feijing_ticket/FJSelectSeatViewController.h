//
//  FJSelectSeatViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-3-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeatButton.h"

@interface FJSelectSeatViewController : UIViewController<UIScrollViewDelegate>{
    UIView *selectSeatView;
}

@property (nonatomic, strong) IBOutlet UIView *selectSeatView;

-(IBAction) selectSeat: (SeatButton *)sender;

@end
