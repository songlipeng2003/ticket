//
//  FJSelectSeatViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-3-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJSelectSeatViewController.h"

@interface FJSelectSeatViewController ()

@end

@implementation FJSelectSeatViewController

@synthesize selectSeatView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    selectSeatView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 950.0f, 550.0f)];
    selectSeatView.contentMode = UIViewContentModeCenter;
    scrollView.contentSize = selectSeatView.frame.size;
    scrollView.frame = CGRectMake(0.0f, 30.0f, 320.0f, 250.0f);
    scrollView.maximumZoomScale = 3.0f;
    scrollView.minimumZoomScale = 0.5f;
    scrollView.delegate = self;
    
    for (int i=0; i<20; i++) {
        for (int j=0; j<20; j++) {
            SeatButton *button = [[SeatButton alloc]initWithFrame:CGRectMake(40.0f+45.0f*i, 10.0f+25.0f*j, 40.0f, 20.0f)];
            [button setTitle:@"Seat" forState:UIControlStateNormal];
            [button addTarget:self action:@selector(selectSeat:) forControlEvents:UIControlEventTouchDown];
            [selectSeatView addSubview:button];
            
            if (i==0) {
                UILabel *rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 10.0f+25.0f*j, 20.0f, 15.0f)];
                rowLabel.text = [NSString stringWithFormat:@"%d", j+1];
                [selectSeatView addSubview:rowLabel];
            }
            
            if(j==19){
                UILabel *rowLabel = [[UILabel alloc] initWithFrame:CGRectMake(40.0f+45.0f*i, 520.0f, 40.0f, 15.0f)];
                rowLabel.text = [NSString stringWithFormat:@"%d", i+1];
                [selectSeatView addSubview:rowLabel];
            }
        }
    }
    
    [scrollView addSubview:selectSeatView];
    [self.view addSubview:scrollView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

#pragma mark - UIScrollViewDelegate
- (UIView*) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return selectSeatView;
}

#pragma mark -
-(IBAction) selectSeat: (SeatButton *)sender{
    sender.selectSeat = sender.selectSeat==YES?NO:YES;
}

@end
