//
//  FJTheatreDetailViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theatre.h"
#import "Movie.h"

@interface FJTheatreDetailViewController : UITableViewController{
    Theatre *theatre;
    UINavigationItem *navigationItemTitle;
    NSMutableArray *moviesInTheatre;
    Movie *currentMovie;
}

@property (nonatomic, strong) Theatre *theatre;
@property (nonatomic, strong) NSMutableArray *moviesInTheatre;
@property (nonatomic, strong) Movie *currentMovie;

@property (nonatomic, strong) IBOutlet UINavigationItem *navigationItemTitle;

- (void)configureView;

@end
