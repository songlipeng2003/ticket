//
//  FJTheatreDetailViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJTheatreDetailViewController.h"
#import "FJTheatreMovieDetailViewController.h"

@implementation FJTheatreDetailViewController

@synthesize theatre;
@synthesize navigationItemTitle;
@synthesize moviesInTheatre;
@synthesize currentMovie;

- (void)setTheatre:(Theatre *)newTheatre
{
    if (theatre != newTheatre) {
        theatre = newTheatre;
        
        // Update the view.
        [self configureView];
    }  
}

- (void)configureView{
    // Update the user interface for the detail item.
    
    if (self.theatre) {
        navigationItemTitle.title = theatre.name;
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    moviesInTheatre = [[NSMutableArray alloc] init];
    
    int size = arc4random()%10;
    
    for(int i=0;i<=size;i++){
        NSString *number = [NSString stringWithFormat:@"%d", i];
        Movie *movie = [[Movie alloc] init];
        movie.title = [@"Movie " stringByAppendingString:number];
        
        [self.moviesInTheatre addObject:movie];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

# pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier;
    
    if(indexPath.section==0){
        if(indexPath.row==0){
            cellIdentifier = @"theatreAddress";
        }else if(indexPath.row==1){
            cellIdentifier = @"theatrePhone";
        }
    }else if(indexPath.section==1){
        cellIdentifier = @"theatreMovie";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(indexPath.section==0){
        if(indexPath.row==0){
            cell.textLabel.text = theatre.address;
        }else if(indexPath.row==1){
            cell.textLabel.text = theatre.phone;;
        }
    }else if(indexPath.section==1){
        cell.textLabel.text = @"Movie";
        
        cell.detailTextLabel.text = @"曾荣获奥斯卡金像奖的著名导演安德鲁·斯坦顿带来全新力作《异星战场》，一部以神秘壮丽的火星为背景的史诗动作冒险巨制。";
        [cell.detailTextLabel setLineBreakMode:UILineBreakModeWordWrap];
        [cell.detailTextLabel setNumberOfLines:0];
        //NSURL *url = [NSURL URLWithString:movie.image];
        //NSData *data = [NSData dataWithContentsOfURL:url];
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"movie" ofType:@"jpg"]];
        UIImage *image = [[UIImage alloc] initWithData:data];
        cell.imageView.image = image;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return 2;
    }else{
        return moviesInTheatre.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section==0){
        return @"电影院信息";
    }else if(section==1){
        return @"电影";
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section!=0){
        return 120.0f;
    }else{
        return 44.0f;
    }
}

#pragma mark - 
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"tableToThreatreMovieDetail"]){
        id controller = segue.destinationViewController;
        
        if([controller isKindOfClass:[FJTheatreMovieDetailViewController class]]){
            FJTheatreMovieDetailViewController *threatreMovieDetailViewController = segue.destinationViewController;
            
            threatreMovieDetailViewController.theatre = theatre;
            threatreMovieDetailViewController.movie = currentMovie;
        }
    }
}

@end
