//
//  FJTheatreMapView.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "FJMapButton.h"
#import "FJTheatreDetailViewController.h"

@interface FJTheatreMapView : MKMapView <CLLocationManagerDelegate,MKMapViewDelegate>{
    NSArray *theatres;
    FJMapButton *mapButton;
}

@property (nonatomic, strong) NSArray *theatres;

@property (nonatomic, strong) IBOutlet FJMapButton *mapButton;

- (IBAction)mapButtonAction:(id)sender;

@end
