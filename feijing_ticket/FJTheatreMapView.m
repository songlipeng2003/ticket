//
//  FJTheatreMapView.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJTheatreMapView.h"
#import "Theatre.h"
#import "FJTheatreViewController.h"
#import "FJTheatreDetailViewController.h"
#import "FJTheatrePointAnnotation.h"

@implementation FJTheatreMapView

@synthesize theatres;
@synthesize mapButton;

#pragma  - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
}

#pragma - MKMapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]]){
        return nil;
    }
    
    // Handle any custom annotations.
    if ([annotation isKindOfClass:[FJTheatrePointAnnotation class]]){
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView* annotationView = (MKPinAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"theatreAnnotation"];
        
        if (!annotationView){
            FJTheatrePointAnnotation *theatrePointAnnotation = (FJTheatrePointAnnotation *)annotation;
            
            // If an existing pin view was not available, create one
            annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:theatrePointAnnotation reuseIdentifier:@"theatreAnnotation"];
            annotationView.pinColor = MKPinAnnotationColorRed;
            annotationView.animatesDrop = YES;
            annotationView.canShowCallout = YES;
            
            // Add a detail disclosure button to the callout.
            mapButton.theatre = theatrePointAnnotation.theatre;
            
            annotationView.rightCalloutAccessoryView = mapButton;
        }else{
            annotationView.annotation = annotation;
        }
        
        return annotationView;
    }
    
    return nil;
}

- (IBAction)mapButtonAction:(id)sender{
    FJMapButton *clickMapButton = (FJMapButton *)sender;
    
    id controller = self.nextResponder;
    
    if([controller isKindOfClass:[FJTheatreViewController class]]){
        FJTheatreViewController *theatreViewController = (FJTheatreViewController *)controller;
        
        theatreViewController.currentTheatre = clickMapButton.theatre;
    }
}

@end
