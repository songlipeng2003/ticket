//
//  FJthreatreMovieDetailViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Movie.h"
#import "Theatre.h"

@interface FJTheatreMovieDetailViewController : UITableViewController{
    Movie *movie;
    Theatre *theatre;
    UIView *movieTimeView;
}

@property (nonatomic, strong) Movie *movie;
@property (nonatomic, strong) Theatre *theatre;
@property (nonatomic, strong) IBOutlet UIView *movieTimeView;

-(IBAction) selectTime:(id)sender;

@end
