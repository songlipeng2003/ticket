//
//  FJthreatreMovieDetailViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJTheatreMovieDetailViewController.h"

@implementation FJTheatreMovieDetailViewController

@synthesize movie;
@synthesize theatre;
@synthesize movieTimeView;

- (void)setTheatre:(Theatre *)newTheatre
{
    if (theatre != newTheatre) {
        theatre = newTheatre;
        
        // Update the view.
        //[self configureView];
    }  
}

- (void)setMovie:(Movie *)newMovie
{
    if (movie != newMovie) {
        movie = newMovie;
        
        // Update the view.
        //[self configureView];
    }  
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(10.0f, 10.0f, 60.0f, 30.0f);
    [button setTitle:@"12:00" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(selectTime:) forControlEvents:UIControlEventTouchDown];
    [movieTimeView addSubview:button];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button1 setTitle:@"12:00" forState:UIControlStateNormal];
    button1.frame = CGRectMake(70.0f, 10.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button1];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button2 setTitle:@"12:00" forState:UIControlStateNormal];
    button2.frame = CGRectMake(130.0f, 10.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button2];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button3 setTitle:@"12:00" forState:UIControlStateNormal];
    button3.frame = CGRectMake(190.0f, 10.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button3];
    
    UIButton *button4 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button4 setTitle:@"12:00" forState:UIControlStateNormal];
    button4.frame = CGRectMake(250.0f, 10.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button4];
    
    UIButton *button5 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button5 setTitle:@"12:00" forState:UIControlStateNormal];
    button5.frame = CGRectMake(10.0f, 40.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button5];
    
    UIButton *button6 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button6 setTitle:@"12:00" forState:UIControlStateNormal];
    button6.frame = CGRectMake(70.0f, 40.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button6];
    
    UIButton *button7 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button7 setTitle:@"12:00" forState:UIControlStateNormal];
    button7.frame = CGRectMake(130.0f, 40.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button7];
    
    UIButton *button8 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button8 setTitle:@"12:00" forState:UIControlStateNormal];
    button8.frame = CGRectMake(190.0f, 40.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button8];
    
    UIButton *button9 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button9 setTitle:@"12:00" forState:UIControlStateNormal];
    button9.frame = CGRectMake(250.0f, 40.0f, 60.0f, 30.0f);
    [movieTimeView addSubview:button9];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

# pragma mark - UITableViewDelegate
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if(section==1){
        return movieTimeView;
    }else if(section==2){
        return movieTimeView;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if(section==1){
        return 80.0f;
    }else if(section==2){
        return 80.0f;
    }
    return 0.0f;
}

# pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentifier;
    
    if(indexPath.section==0){
        if(indexPath.row==0){
            cellIdentifier = @"theatreAddress";
        }else if(indexPath.row==1){
            cellIdentifier = @"theatrePhone";
        }
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(indexPath.section==0){
        if(indexPath.row==0){
            cell.textLabel.text = theatre.address;
        }else if(indexPath.row==1){
            cell.textLabel.text = theatre.phone;;
        }
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section==0){
        return 2;
    }else{
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if(section==0){
        return @"电影院信息";
    }else if(section==1){
        return @"电影排期";
    }
    return nil;
}

#pragma mark -
-(IBAction) selectTime:(id)sender{
    [self performSegueWithIdentifier:@"selectTime" sender:self];
}

@end
