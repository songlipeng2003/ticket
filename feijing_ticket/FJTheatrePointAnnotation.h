//
//  FJTheatrePointAnnotation.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Theatre.h"

@interface FJTheatrePointAnnotation : NSObject <MKAnnotation>{
    NSString *subTitle;
    NSString *title;
    CLLocationCoordinate2D coordinate;
}

@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) Theatre *theatre;

@end
