//
//  FJTheatrePointAnnotation.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-10.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJTheatrePointAnnotation.h"

@implementation FJTheatrePointAnnotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;
@synthesize theatre;

@end
