//
//  FJTheatreTableView.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FJTheatreTableView : UITableView<UITableViewDelegate,UITableViewDataSource>{
    NSArray *theatres;
}

@property (nonatomic, strong) NSArray *theatres;

@end
