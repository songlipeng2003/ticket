//
//  FJTheatreTableView.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "FJTheatreTableView.h"
#import "Theatre.h"
#import "FJTheatreViewController.h"
#import "FJTheatreDetailViewController.h"

@implementation FJTheatreTableView

@synthesize theatres;

# pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"theatreTableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Theatre *threatre = [theatres objectAtIndex:[indexPath row]];
    
    cell.textLabel.text = threatre.name;
    cell.detailTextLabel.text = threatre.address;
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger count =  [self.theatres count];
    return count;
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSUInteger index = [indexPath row];
    Theatre *selectedTheatre = [theatres objectAtIndex:index];
    
    id controller = self.nextResponder;
    
    if([controller isKindOfClass:[FJTheatreViewController class]]){
        FJTheatreViewController *theatreViewController = (FJTheatreViewController *)controller;
        
        theatreViewController.currentTheatre = selectedTheatre;
    }
    
    return indexPath;
}

@end
