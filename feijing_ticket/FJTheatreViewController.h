//
//  FJSecondViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Theatre.h"
#import "FJTheatreMapView.h"
#import "FJTheatreTableView.h"

@interface FJTheatreViewController : UIViewController{
    NSMutableArray *theatres;
    FJTheatreMapView *theatreMapView;
    FJTheatreTableView *theatreTableView;
    Theatre *currentTheatre;
    UIButton *cityButton;
}

@property (nonatomic, strong) NSMutableArray *theatres;
@property (nonatomic, strong) Theatre *currentTheatre;

@property (nonatomic, strong) IBOutlet FJTheatreMapView *theatreMapView;
@property (nonatomic, strong) IBOutlet FJTheatreTableView *theatreTableView;
@property (nonatomic, strong) IBOutlet UIButton *cityButton;

-(IBAction)buttonAction: (UISegmentedControl *) sender;

@end
