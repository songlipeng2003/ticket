//
//  FJSecondViewController.m
//  feijing_ticket
//
//  Created by song thinking on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "FJTheatreViewController.h"
#import "FJTheatreDetailViewController.h"
#import "FJTheatrePointAnnotation.h"

@implementation FJTheatreViewController

@synthesize theatres;
@synthesize theatreMapView;
@synthesize theatreTableView;
@synthesize currentTheatre;
@synthesize cityButton;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.theatres = [[NSMutableArray alloc] init];
    
    for(int i=0;i<10;i++){
        Theatre *theatre = [[Theatre alloc] init];
        
        NSString *number = [NSString stringWithFormat:@"%d", i];
        
        theatre.name = [number stringByAppendingString:@" Theatre"];
        theatre.address = @"北京市中关村大街50号";
        theatre.phone = @"15010091144";
        theatre.longitude = [[NSNumber alloc] initWithFloat:(39.510518 + (float)(arc4random()%1000)/10000)];
        theatre.latitude = [[NSNumber alloc] initWithFloat:(116.09825 + (float)(arc4random()%1000)/10000)];
        
        [self.theatres addObject:theatre];
    }
    
    // tableView
    theatreTableView.theatres = [self.theatres copy];
    theatreTableView.delegate = theatreTableView;
    theatreTableView.dataSource = theatreTableView;
    
    // mapView
    theatreMapView.theatres = [self.theatres copy];
    theatreMapView.showsUserLocation=YES;
    theatreMapView.zoomEnabled = YES;
    theatreMapView.delegate = theatreMapView;
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];//创建位置管理器
    locationManager.delegate=theatreMapView;//设置代理
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;//指定需要的精度级别
    locationManager.distanceFilter=1000.0f;//设置距离筛选器
    [locationManager startUpdatingLocation];//启动位置管理器
    
    //地图的范围 越小越精确
    MKCoordinateSpan theSpan;
    theSpan.latitudeDelta=0.5;
    theSpan.longitudeDelta=0.5;
    MKCoordinateRegion theRegion;
    theRegion.center=[[locationManager location] coordinate];
    theRegion.span=theSpan;
    theatreMapView.region = theRegion;
    
    //添加电影院位置
    for (Theatre *object in self.theatres) {
        CGFloat x = [object.longitude floatValue];
        CGFloat y = [object.latitude floatValue];
        
        CLLocationCoordinate2D loc = CLLocationCoordinate2DMake(x, y);
        
        FJTheatrePointAnnotation *annotion = [[FJTheatrePointAnnotation alloc] init];
        
        annotion.coordinate = loc;
        annotion.title = object.name;
        annotion.subtitle = object.address;
        annotion.theatre = object;
        
        [theatreMapView addAnnotation:annotion];
    }

}

- (void)vewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *currentCity = [[NSUserDefaults standardUserDefaults] stringForKey:@"currentCity"];
    currentCity = currentCity==nil?@"北京":currentCity;
    [cityButton setTitle:currentCity forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

-(IBAction)buttonAction: (UISegmentedControl *) sender  
{  
    //得到按钮点击索引   
    NSInteger index = sender.selectedSegmentIndex;
    
    if(index==1){
        self.view = theatreMapView;
    }else{
        self.view = theatreTableView;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"tableToThreatreDetail"] || [segue.identifier isEqualToString:@"mapToTheatreDetail"]){
        id controller = segue.destinationViewController;
        
        if([controller isKindOfClass:[FJTheatreDetailViewController class]]){
            FJTheatreDetailViewController *theatreDetailViewController = segue.destinationViewController;
            
            theatreDetailViewController.theatre = currentTheatre;
        }
    }
}

@end
