//
//  FJUserInfoViewController.h
//  feijing_ticket
//
//  Created by song thinking on 12-1-26.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FJUserInfoViewController : UITableViewController{
    UILabel *currentCityLabel;
}

@property (nonatomic, strong) IBOutlet UILabel *currentCityLabel;

@end
