//
//  Movie.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSNumber *price;

@end
