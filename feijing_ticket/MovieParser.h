//
//  MovieParser.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Movie.h"

@interface MovieParser : NSObject<NSXMLParserDelegate>{
    NSString *element;
    Movie *movie;
    NSMutableArray *movies;
}

@property (nonatomic, retain) NSMutableArray *movies;

@end
