//
//  MovieParser.m
//  feijing_ticket
//
//  Created by song thinking on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "MovieParser.h"

@implementation MovieParser

@synthesize movies;

- (id)init{
    self = [super init];
    if (self!=nil) {
        NSMutableArray *temp = [[NSMutableArray alloc] init];
        self.movies = temp;
    }
    return self;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    NSLog(@"start %@", elementName);
    element = [elementName copy];
    if ([elementName isEqualToString:@"url"]) {
        movie = [[Movie alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if(element!=nil){
        NSLog(@"element %@ content %@", element, string);
    }
    
    if([element isEqualToString:@"title"]){
        movie.title = string;
    }else if([element isEqualToString:@"image"]){
        movie.image = string;
    }else if([element isEqualToString:@"price"]){
        movie.price = [NSDecimalNumber decimalNumberWithString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    NSLog(@"end %@", elementName);
    element = nil;
    if ([elementName isEqualToString:@"url"]) {
        NSLog(@"add movie");
        [self.movies addObject:movie];
        NSInteger count = [self.movies count];
        NSLog(@"cout of movies: %d", count);
    }
}
@end
