//
//  Theatre.h
//  feijing_ticket
//
//  Created by song thinking on 12-2-2.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Theatre : NSObject{
    NSString *name;
    NSNumber *longitude;
    NSNumber *latitude;
    NSString *address;
    NSString *phone;
}

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *longitude;
@property (nonatomic, copy) NSNumber *latitude;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *phone;

@end
