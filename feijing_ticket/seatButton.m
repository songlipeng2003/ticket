//
//  seatButton.m
//  feijing_ticket
//
//  Created by song thinking on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SeatButton.h"

@implementation SeatButton

@synthesize selectSeat;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor greenColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void) setSelectSeat:(BOOL)newSelectSeat{
    selectSeat = newSelectSeat;
    if(selectSeat){
        self.backgroundColor = [UIColor redColor];
    }else{
        self.backgroundColor = [UIColor greenColor];
    }
}

@end
